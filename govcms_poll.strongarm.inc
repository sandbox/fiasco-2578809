<?php
/**
 * @file
 * govcms_poll.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function govcms_poll_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_poll';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
    2 => 'revision',
  );
  $export['node_options_poll'] = $strongarm;

  return $export;
}
