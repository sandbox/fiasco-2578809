<?php

/**
 * @file
 * Related polls
 *
 * Renders one or more polls that are related to the current content page.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Poll - related'),
  'description' => t('Displays the most recent related polls to a given piece of content.'),
  'category' => t('Māori Television'),
  'edit form' => 'govcms_poll_related_edit_form',
  'admin info' => 'govcms_poll_related_admin_info',
  'render callback' => 'govcms_poll_related_render',
  'defaults' => array(
    'limit' => 1,
  ),
);

/**
 * The 'Edit form' callback for the content type.
 */
function govcms_poll_related_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['limit'] = array(
    '#title' => t('Limit'),
    '#description' => t('How many news related polls to display'),
    '#type' => 'select',
    '#options' => drupal_map_assoc(range(1,4)),
    '#default_value' => $conf['limit'],
    '#required' => TRUE,
  );

  return $form;
}

/**
 * The submit form stores the data in $conf.
 */
function govcms_poll_related_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * The 'admin info' callback for panel pane.
 */
function govcms_poll_related_admin_info($subtype, $conf, $contexts) {
  $block = new stdClass();
  $block->title = '';
  $block->content = format_plural($conf['limit'], 'Displaying the most recent related poll', 'Displaying the @count most recent related polls');
  return $block;
}

/**
 * Run-time rendering of the body of the block (content type).
 *
 * See ctools_plugin_examples for more advanced info
 */
function govcms_poll_related_render($subtype, $conf, $panel_args, $context = NULL) {
  global $language;

  $block = new stdClass();
  $block->title = '';
  $block->content = '';

  if (empty($conf)) {
    return $block;
  }

  // If this is a news article that we are currently on, then see if there are
  // any direct references to this from a poll. All references are to the
  // English version of the poll.
  $node = menu_get_object();
  if (isset($node) && $node->type == 'mts_news_article') {
    // Find the English news article.
    if ($node->language == 'mi') {
      $news_article = translation_helpers_get_translation($node, 'en');
      if (!$news_article) {
        // There is no English translation of this article, but we depend upon
        // the English version, so do nothing.
        return $block;
      }
    }
    else {
      $news_article = $node;
    }

    // Do a lookup to see if there are any polls that have this English node as
    // a reference.
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'node', '=')
      ->entityCondition('bundle', 'poll')
      ->propertyCondition('language', $language->language)
      ->propertyCondition('status', 1)
      ->fieldCondition('field_related_news_article', 'target_id', $news_article->nid)
      ->propertyOrderBy('created', 'DESC')
      ->range(0, $conf['limit'])
      ->addTag('govcms_poll_related');

    // @TODO alter the query to join on the poll table to ensure the poll is
    // active?

    $result = $query->execute();
    if (empty($result)) {
      return $block;
    }

    $polls = node_load_multiple(array_keys($result['node']));

    // Set the title.
    $block->title = format_plural(count($polls), 'Related poll', 'Related polls');

    // Set the content.
    $block->content .= drupal_render(node_view_multiple($polls, 'full'));
  }

  // We could be on a landing page, and there could be a poll that is related to
  // this landing page.
  else if (isset($node) && $node->type == 'news_landing_page') {
    $wrapper = entity_metadata_wrapper('node', $node);

    // First find out the tags this news landing page is tagged with.
    $tags = array();
    foreach ($wrapper->field_news_category->value() as $term) {
      $tags[$term->tid] = $term->name;
    }
    foreach ($wrapper->field_region->value() as $term) {
      $tags[$term->tid] = $term->name;
    }

    if (empty($tags)) {
      return $block;
    }

    // Now we need to find the any active polls that related to a news article,
    // and that news article is tags with at least one of the above tags.
    $nids = govcms_poll_related_poll_nid((int) $conf['limit'], $tags);
    if (empty($nids)) {
      return $block;
    }

    // Set the title.
    $block->title = format_plural(count($nids), 'Related poll', 'Related polls');

    // Return the block content.
    $nodes = node_load_multiple($nids);
    $block->content .= drupal_render(node_view_multiple($nodes, 'full'));
  }

  return $block;
}
