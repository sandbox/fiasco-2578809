<?php
/**
 * @file
 * This is the theme function for the full display of the poll node. This is
 * largely overridden from Drupal core. Features include AJAX for voting and
 * localstorage for the tracking of votes.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see zen_preprocess_node()
 * @see template_process()
 */
?>
<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?> data-nid="<?php print $node->nid; ?>">

  <?php if ($unpublished): ?>
    <div class="alert alert-danger">
      <p><strong><?php print t('Unpublished'); ?></strong></p>
    </div>
  <?php endif; ?>

  <?php if ($page) : ?>
    <?php print render($title_prefix); ?>
    <?php print render($title_suffix); ?>
  <?php else : ?>
    <?php print render($title_prefix); ?>
    <?php print render($title_suffix); ?>
  <?php endif; ?>

  <div class="poll-messages"></div>

  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    hide($content['poll_view_results']);
    hide($content['poll_view_voting']);
    print render($content);
  ?>

  <?php if ($poll_closed) : ?>
    <div class="tab-pane" id="results-<?php print $node->nid; ?>">
      <div class="poll poll-results">
        <?php print $poll_results; ?>
      </div>
    </div>
  <?php else : ?>
    <ul class="nav nav-tabs poll-tabs">
      <li class="active"><a data-toggle="tab" href="#vote-<?php print $node->nid; ?>" class="tab-vote">Options</a></li>
      <li class=""><a data-toggle="tab" href="#results-<?php print $node->nid; ?>" class="tab-results">Results</a></li>
    </ul>

    <div class="tab-content" id="poll-<?php print $node->nid; ?>-tabsContent">

      <div class="tab-pane active" id="vote-<?php print $node->nid; ?>">
        <div class="poll-voting-form">
          <?php print $poll_voting_form; ?>
        </div>
      </div>

      <div class="tab-pane" id="results-<?php print $node->nid; ?>">
        <div class="poll poll-results">
          <?php print $poll_results; ?>
        </div>
      </div>

    </div>
  <?php endif; ?>

</article><!-- /.node -->
