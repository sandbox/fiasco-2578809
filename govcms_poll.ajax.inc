<?php

/**
 * @file
 * Has all the AJAX functionality needed for polls on the site.
 */

/**
 * Records a vote against a poll.
 */
function govcms_poll_ajax_vote($node) {
  global $user;

  $params = drupal_get_query_parameters($_POST);
  if (!isset($params['choice'])) {
    drupal_json_output(array('status' => 0, 'messages' => t('You must choose something.')));
    drupal_exit();
  }

  // Work out if this is radio buttons or checkboxes.
  $number_of_options_allowed = (int) $node->field_options[LANGUAGE_NONE][0]['value'];

  // Work out if the user has submitted too many answers here.
  if (count($params['choice']) > $number_of_options_allowed) {
    drupal_json_output(array(
      'status' => 0,
      'messages' => t('You are only allowed to choose a maximum of @count options.', array(
        '@count' => $number_of_options_allowed,
      )),
    ));
    drupal_exit();
  }

  // Normalise the data from the user into integers.
  $choice = array();
  if ($number_of_options_allowed === 1) {
    $choice[] = (int) $params['choice'];
  }
  else {
    foreach ($params['choice'] as $vote) {
      $choice[] = (int) $vote;
    }
  }

  // Error checking on the vote ID here.
  foreach ($choice as $vote) {
    $valid = (bool) db_query('SELECT 1 FROM {poll_choice} WHERE chid = :chid', array(':chid' => $vote))->fetchField();
    if ($valid == false)  {
      drupal_json_output(array('status' => 0, 'messages' => t('Invalid choice.')));
      drupal_exit();
    }
  }

  // Everything is valid, insert the data into the database.
  foreach ($choice as $vote) {
    db_insert('poll_vote')
      ->fields(array(
        'nid' => $node->nid,
        'chid' => $vote,
        'uid' => $user->uid,
        'hostname' => ip_address(),
        'timestamp' => REQUEST_TIME,
      ))
      ->execute();

    // Add one to the votes.
    db_update('poll_choice')
      ->expression('chvotes', 'chvotes + 1')
      ->condition('chid', $vote)
      ->execute();
  }

  drupal_json_output(array(
    'status' => 1,
    'messages' => format_plural(count($choice), 'Your choice was recorded.', 'Your @count choices were recorded.'),
    'output' => poll_view_results($node, 'full'),
  ));

  drupal_exit();
}

/**
 * This is not done with Drupal theme function and template as this is called
 * over a lightweight bootstrap and the theme layer is not yet loaded.
 */
function govcms_poll_theme_poll_results($nid) {
  $output = '';

  // Find the translation pair for this poll (if it exists).
  $tnid = db_query('SELECT tnid FROM {node} WHERE nid = :nid', array(':nid' => $nid))->fetchField();

  // More than one poll in the translation set. Merge the votes from both polls.
  $votes = array();
  if (!empty($tnid) && $tnid > 0) {
    $nids = db_query('SELECT nid FROM {node} WHERE tnid = :tnid', array(':tnid' => $tnid))->fetchCol();
    $votes_both = db_query('SELECT chid, chtext, chvotes, nid, weight FROM {poll_choice} WHERE nid IN (:nids) ORDER BY weight ASC', array(':nids' => $nids))->fetchAllAssoc('chid');

    // We need to add up the votes with the same weight. Start with the
    // language you are currently in.
    foreach ($votes_both as $chid => $vote) {
      if ($vote->nid == $nid) {
        $votes[$vote->weight] = $vote;
      }
    }

    // Add in the other language's votes.
    foreach ($votes_both as $chid => $vote) {
      if ($vote->nid != $nid) {
        $votes[$vote->weight]->chvotes += $vote->chvotes;
      }
    }
  }

  // Only a single language.
  else {
    $votes = db_query('SELECT chid, chtext, chvotes FROM {poll_choice} WHERE nid = :nid ORDER BY weight ASC', array(':nid' => $nid))->fetchAllAssoc('chid');
  }

  // Find out if the poll is active, if it is not, then we want to add
  // additional markup to the results to indicate this.
  $active = (bool) db_query('SELECT active from {poll} WHERE nid = :nid', array(':nid' => $nid))->fetchField();
  $non_active_text = '';
  if (!$active) {
    $non_active_text = ' &ndash; ' . t('(poll is closed)');
  }

  $total_votes = 0;
  foreach ($votes as $chid => $vote) {
    $total_votes += $vote->chvotes;
  }

  foreach ($votes as $vote) {
    // Try to not divide by zero.
    $percent = $total_votes > 0 ? number_format($vote->chvotes / $total_votes * 100, 0) : 0;
    $output .= '
      <p>
         <span class="poll-title">' . check_plain($vote->chtext) . '</span>
         <span class="poll-percent">' . $percent . '%</span>
      </p>
      <div class="progress">
        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="' . $percent . '" aria-valuemin="0" aria-valuemax="' . $total_votes . '" style="width: ' . $percent . '%">
          <span class="sr-only">' . $percent . '% ' . format_plural($vote->chvotes, '1 response', '@count responses') . '</span>
        </div>
      </div>';
  }
  $output .=
    '<p class="poll-total">' .
      t('Total responses: @votes', array('@votes' => $total_votes)) . $non_active_text .
    '</p>';

  return $output;
}


/**
 * Returns the results for a particular poll, this does not use a theme function
 * for ultimate speed. The entire AJAX callback does only 6 SQL queries for
 * anonymous users.
 */
function govcms_poll_ajax_results() {
  drupal_add_http_header('Content-Type', 'text/html');
  $ttl = variable_get('govcms_poll_results_maximum_age', 10);
  drupal_add_http_header('Expires', date('r', strtotime('+' . $ttl . ' seconds')));
  drupal_add_http_header('Cache-Control', 'public, max-age=' . $ttl);

  $nid = arg(3);
  $output = govcms_poll_theme_poll_results($nid);

  print $output;
  drupal_exit();
}
