/**
 * Check local storage to see if the user has voted, and if they have not, to
 * hide the results, and use AJAX to retrieve the poll voting form.
 */

(function ($) {

Drupal.behaviors.govcms_poll = {
  attach: function (context, settings) {

    var _localStorageAvailable = isLocalStorageAvailable(),
        _keyPrefix = 'govcms_poll_';

    // Loop through each poll and see if a poll voting form needs to be
    // rendered.
    $('.node-poll').each(function(index) {
      var nid = $(this).data('nid');
      if (_localStorageAvailable) {

        // Define some variables.
        var $pollForm = $('.node-' + nid + ' .poll-voting-form');
        var $pollFormTab = $('.node-' + nid + ' .tab-vote');
        var $pollResults = $('.node-' + nid + ' .poll-results');
        var $pollResultsTab = $('.node-' + nid + ' .tab-results');
        var $pollMessages = $('.node-' + nid + ' .poll-messages');
        var $form = $('.node-' + nid + ' .poll-voting-form form');
        var enabledText = $form.find('input.form-submit').val();

        // Already voted, refresh the results table from the website.
        if (localStorage.getItem(_keyPrefix + nid)) {
          $pollForm.hide();
          $pollResultsTab.click();
          $pollFormTab.addClass('disabled').attr('data-toggle', '');
          updatePollResults(nid);
          $pollMessages.html(Drupal.theme('ajaxPollSuccess', Drupal.t('Your choice is already recorded')));
        }

        // Not yet voted, show the poll voting form, hide the results.
        else {
          $pollForm.show();
          $pollFormTab.click();

          // Set up the options for the AJAX voting mechanism.
          var options = {
            url: settings.govcms_poll.ajax_voting_path + '/' + nid,
            beforeSubmit: function(values, form, options) {
              $form.find('input.form-submit').attr('disabled', true).val(settings.govcms_poll.voting_message);
            },
            success: function(response, status) {
              // Remove previous messages and re-enable the buttons in case anything
              // goes wrong after this.
              $form.find('input.form-submit').attr('disabled', '').val(enabledText);
              $form.find('.messages').remove();

              // On success, replace the poll content with the new content.
              if (response.status) {
                $pollForm.hide();
                $pollResultsTab.click();
                $pollFormTab.addClass('disabled').attr('data-toggle', '');
                updatePollResults(nid);

                // Stop further votes.
                localStorage.setItem(_keyPrefix + nid, '1');

                // Display any new messages.
                if (response.messages) {
                  $pollMessages.html(Drupal.theme('ajaxPollSuccess', response.messages));

                  // Check for GA.
                  if (typeof ga == 'function') {
                    ga('send', 'event', 'poll', 'vote-success', 'Poll - ' + nid);
                  }
                }
              }

              // Poll vote was not recorded for some reason.
              else {
                if (response.messages) {
                  $pollMessages.html(Drupal.theme('ajaxPollError', response.messages));

                  // Check for GA.
                  if (typeof ga == 'function') {
                    ga('send', 'event', 'poll', 'vote-error', 'Poll - ' + nid);
                  }
                }
              }
            },
            complete: function(response, status) {
              $form.find('input.form-submit').attr('disabled', '').val(enabledText);

              if (status == 'error' || status == 'parsererror') {
                $pollMessages.html(Drupal.theme('ajaxPollError'));
              }
            },
            dataType: 'json',
            type: 'POST'
          };

          // Add the handlers to the Poll form through the jquery.form.js library.
          $form.ajaxForm(options);
        }
      }
    });
  }
};

/**
 * Simple check for local storage.
 */
function isLocalStorageAvailable() {
  if (!(JSON.stringify && JSON.parse)) {
    return false;
  }

  try {
    return 'localStorage' in window && window.localStorage !== null;
  }
  catch (pError) {
    return false;
  }
}

/**
 * Quickly update the poll results with AJAX.
 */
function updatePollResults(nid) {
  $.get(Drupal.settings.govcms_poll.ajax_results_path + '/' + nid, function(data) {
    $('.node-' + nid + ' .poll-results').html(data);
  });
}

/**
 * A fallback error that is shown upon a complete failure.
 */
Drupal.theme.prototype.ajaxPollError = function(msg) {
  return '<div class="alert alert-danger">' + msg || Drupal.settings.govcms_poll.error_message + '</div>';
};
Drupal.theme.prototype.ajaxPollSuccess = function(msg) {
  return '<div class="alert alert-success">' + msg + '</div>';
};

})(jQuery);
